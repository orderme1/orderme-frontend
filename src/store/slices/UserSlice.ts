import { AsyncThunk, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import api from "../../utils/api/api";
import { AsyncThunkConfig } from "@reduxjs/toolkit/dist/createAsyncThunk";

export const registerUser: AsyncThunk<any, User, AsyncThunkConfig> = createAsyncThunk(
    "user/registerUser",
    async (registerForm, {rejectWithValue}) => {
        const response = await api.post("/users/registration", registerForm);
        console.log("the registration form is ", response.data);
        return response.data;
    }
);

export type User = {
    firstName: string,
    middleName: string,
    lastName: string,
    email: string,
    phoneNumber: string,
    role: string
}

type State = {
    loading: boolean,
    user: null | User,
    error: undefined | null | string
}

const initialState: State = {
    loading: false,
    user: null,
    error: null
}

const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
    },
    extraReducers: (builder) => {
        builder.
        addCase(registerUser.pending, (state) => {
            state.loading = true;
            state.user = null;
            state.error = null;
            console.log("pending...")
        }).addCase(registerUser.fulfilled, (state, action) => {
            state.loading = false;
            console.log("The action is ", action)
            state.user = action.payload;
            state.error = null;
            console.log("fulfilled.");
        }).addCase(registerUser.rejected, (state, action) => {
            state.loading = false;
            state.user = null;
            console.log("rejected")
            state.error = action.error.message;
        })
    }
});

export default userSlice.reducer;