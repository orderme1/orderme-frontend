import React, { useState } from 'react'
import "./Registration.css"
import api from '../../utils/api/api';
import { useDispatch } from 'react-redux';
import { User, registerUser } from '../../store/slices/UserSlice';

const Registration = () => {
    const [formData, setFormData] = useState<User>({
        firstName: '',
        middleName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        role: ''
      });

      const dispatch: any = useDispatch();
    
      const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormData({
          ...formData,
          [name]: value
        });
      };
    
      const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        dispatch(registerUser(formData));
      };
    
      return (
        <form onSubmit={handleSubmit}>
      <div className="form-row">
        <label>First Name:</label>
        <input
          type="text"
          name="firstName"
          value={formData.firstName}
          onChange={handleChange}
          placeholder="Enter your first name"
        />
      </div>
      <div className="form-row">
        <label>Middle Name:</label>
        <input
          type="text"
          name="middleName"
          value={formData.middleName}
          onChange={handleChange}
          placeholder="Enter your middle name"
        />
      </div>
      <div className="form-row">
        <label>Last Name:</label>
        <input
          type="text"
          name="lastName"
          value={formData.lastName}
          onChange={handleChange}
          placeholder="Enter your last name"
        />
      </div>
      <div className="form-row">
        <label>Email:</label>
        <input
          type="email"
          name="email"
          value={formData.email}
          onChange={handleChange}
          placeholder="Enter your email"
        />
      </div>
      <div className="form-row">
        <label>Phone Number:</label>
        <input
          type="tel"
          name="phoneNumber"
          value={formData.phoneNumber}
          onChange={handleChange}
          placeholder="Enter your phone number"
        />
      </div>
      <div className="form-row">
        <label>Role:</label>
        <input
          type="text"
          name="role"
          value={formData.role}
          onChange={handleChange}
          placeholder="Enter your role"
        />
      </div>
      <button type="submit">Submit</button>
    </form>
      );
}

export default Registration