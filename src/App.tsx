import './App.css';
import Registration from './components/auth/Registration';

function App() {
  return (
    <div className="App">
      <Registration/>
    </div>
  );
}

export default App;
